<?php

namespace Drupal\config_update_bulk\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\config_update\ConfigListInterface;
use Drupal\config_update\ConfigRevertInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form for reverting configuration.
 */
class ConfigBulkUpdateForm extends FormBase {
  /**
   * The config lister.
   *
   * @var \Drupal\config_update\ConfigListInterface
   */
  protected $configList;

  /**
   * The config reverter.
   *
   * @var \Drupal\config_update\ConfigRevertInterface
   */
  protected $configRevert;

  /**
   * Constructs a ConfigRevertConfirmForm object.
   *
   * @param \Drupal\config_update\ConfigListInterface $config_list
   *   The config lister.
   * @param \Drupal\config_update\ConfigRevertInterface $config_update
   *   The config reverter.
   */
  public function __construct(ConfigListInterface $config_list, ConfigRevertInterface $config_update) {
    $this->configList = $config_list;
    $this->configRevert = $config_update;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config_update.config_list'),
      $container->get('config_update.config_update')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    static $count = 0;
    return 'config_bulk_update' . ++$count;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, array $names = [], $storage = 'active', array  $actions = []) {
    $bulk_actions = array_intersect($actions, ['revert', 'import', 'delete']);

    $action_labels = [
      'export' => $this->t('Export'),
      'import' => $this->t('Import from source'),
      'diff' => $this->t('Show differences'),
      'revert' => $this->t('Revert to source'),
      'delete' => $this->t('Delete'),
    ];

    $form['table']['#type'] = 'tableselect';

    $form['table']['#attributes'] = ['class' => ['config-update-report']];

    $form['table']['#header'] = [
      'name' => [
        'data' => $this->t('Machine name'),
      ],
      'label' => [
        'data' => $this->t('Label (if any)'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'type' => [
        'data' => $this->t('Type'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      'operations' => [
        'data' => $this->t('Operations'),
      ],
    ];

    $form['options'] = [
      '#type' => 'value',
      '#value' => [],
    ];

    $form['table']['#rows'] = [];
    $form['table']['#empty'] = $this->t('No configuration found.');

    foreach ($names as $name) {
      $row = [];
      if ($storage == 'active') {
        $config = $this->configRevert->getFromActive('', $name);
      }
      else {
        $config = $this->configRevert->getFromExtension('', $name);
      }

      // Figure out what type of config it is, and get the ID.
      $entity_type = $this->configList->getTypeNameByConfigName($name);

      if (!$entity_type) {
        // This is simple config.
        $id = $name;
        $type_label = $this->t('Simple configuration');
        $entity_type = 'system.simple';
      }
      else {
        $definition = $this->configList->getType($entity_type);
        $id_key = $definition->getKey('id');
        $id = $config[$id_key];
        $type_label = $definition->getLabel();
      }

      $label = (isset($config['label'])) ? $config['label'] : '';
      $row['name'] = $name;
      $row['label'] = $label;
      $row['type'] = $type_label;

      $links = [];
      $routes = [
        'export' => 'config.export_single',
        'import' => 'config_update_ui.import',
        'diff' => 'config_update_ui.diff',
        'revert' => 'config_update_ui.revert',
        'delete' => 'config_update_ui.delete',
      ];

      foreach ($actions as $action) {
        $links[$action] = [
          'url' => Url::fromRoute($routes[$action], [
            'config_type' => $entity_type,
            'config_name' => $id
          ]),
          'title' => $action_labels[$action],
        ];
      }

      $row['operations'] = [
        'data' => [
          '#type' => 'operations',
          '#links' => $links,
        ],
      ];

      $form['options']['#value'][] = [
        'config_type' => $entity_type,
        'config_name' => $id
      ];

      $form['table']['#options'][] = $row;
    }

    if (!empty($form['table']['#options'])) {
      $form['actions'] = ['#type' => 'actions'];

      foreach ($bulk_actions as $action) {
        $form['actions'][$action] = [
          '#type' => 'submit',
          '#button_type' => 'primary',
          '#dropbutton' => count($bulk_actions) > 1 ? 'submit' : NULL,
          '#value' => $action_labels[$action],
          '#name' => 'bulk_' . $action,
          '#op' => $action,
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    foreach (array_filter($values['table']) as $delta => $config) {
      $config = $values['options'][$delta];
      $value = $this->configRevert->getFromExtension($config['config_type'], $config['config_name']);
      if (!$value) {
        $form_state->setErrorByName('', $this->t('There is no configuration @type named @name to import', [
          '@type' => $config['config_type'],
          '@name' => $config['config_name']
        ]));
        return;
      }
    }


  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    $values = $form_state->getValues();

    $configs = array_filter($values['table']);

    switch ($button['#op']) {
      case 'import':
        foreach ($configs as $delta => $config) {
          $config = $values['options'][$delta];
          $this->configRevert->import($config['config_type'], $config['config_name']);
        }
        drupal_set_message($this->t('@count configurations has been imported.', [
          '@count' => count($configs)
        ]));
        break;
      case 'revert':
        foreach ($configs as $delta => $config) {
          $config = $values['options'][$delta];
          $this->configRevert->revert($config['config_type'], $config['config_name']);
        }
        drupal_set_message($this->t('@count configurations has been reverted to its source.', [
          '@count' => count($configs)
        ]));
        break;
      case 'delete':
        foreach ($configs as $delta => $config) {
          $config = $values['options'][$delta];
          $this->configRevert->delete($config['config_type'], $config['config_name']);
        }
        drupal_set_message($this->t('@count configurations has been deleted.', [
          '@count' => count($configs)
        ]));
        break;
    }
  }
}
