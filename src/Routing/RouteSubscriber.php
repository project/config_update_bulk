<?php
/**
 * @file
 * Contains \Drupal\config_update_bulk\Routing\RouteSubscriber.
 */

namespace Drupal\config_update_bulk\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('config_update_ui.report')) {
      $route->setDefault('_controller', '\Drupal\config_update_bulk\Controller\ConfigBulkUpdateController::report');
    }
  }
}
