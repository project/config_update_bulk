<?php

namespace Drupal\config_update_bulk\Controller;

use Drupal\config_update_ui\Controller\ConfigUpdateController;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;

/**
 * Returns responses for Configuration Revert module operations.
 */
class ConfigBulkUpdateController extends ConfigUpdateController {

  /**
   * {@inheritdoc}
   *
   * Wrap all elements around expandable details elements.
   */
  protected function generateReport($report_type, $value) {
    $build = parent::generateReport($report_type, $value);

    $regions = [
      'removed',
      'inactive',
      'added',
      'different'
    ];

    foreach ($regions as $region) {
      if (isset($build[$region])) {
        // Use empty message from ConfigUpdateController.
        $build[$region]['table']['#empty'] = $build[$region]['#empty'];

        // Wrap around details element and use ConfigUpdateController title.
        $build[$region] = [
          '#type' => 'details',
          '#title' => $build[$region]['#caption'],
          '#open' => empty($build[$region]['form']['#options']),
          'form' => $build[$region],
        ];
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * Instead building a table building a form with a tableselect element.
   */
  protected function makeReportTable($names, $storage, $actions) {
    return \Drupal::formBuilder()
      ->getForm('\Drupal\config_update_bulk\Form\ConfigBulkUpdateForm', $names, $storage, $actions);
  }
}
